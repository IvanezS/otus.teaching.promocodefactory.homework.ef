﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly EfCustomerRepository _customerRep;
        private readonly IRepository<Preference> _preferenceRep;
        private readonly IRepository<PromoCode> _promoCodeRep;


        public CustomersController(EfCustomerRepository customerRep,  IRepository<Preference> preferenceRep, IRepository<PromoCode> promoCodeRep)
        {
            _customerRep = customerRep;
            _preferenceRep = preferenceRep;
            _promoCodeRep = promoCodeRep;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRep.GetAllAsync();
            var customersToShort = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }
            ).ToList();
            return Ok(customersToShort); 
        }

        /// <summary>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRep.GetByIdAsync(id);

            if (customer == null) return NotFound();

            var customerRespponse = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,

            };

            customerRespponse.Preferences = customer.CustomerPreferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId,
                Name = x.preferencess.Name
            }).ToList();

            customerRespponse.PromoCodes = customer.PromoCodes.Select(y => new PromoCodeShortResponse()
            {
                Id = y.Id,
                Code = y.Code,
                PartnerName = y.PartnerName,
                ServiceInfo = y.ServiceInfo,
                BeginDate = y.BeginDate.ToString("yyyy.MM.dd HH:mm:ss"),
                EndDate = y.EndDate.ToString("yyyy.MM.dd HH:mm:ss")

            }).ToList();
            

            return Ok(customerRespponse);
        }

        /// <summary>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            
            var preferences = await _preferenceRep.GetListByListId(request.PreferenceIds);

            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };

            if (preferences != null && preferences.Count() > 0)
            {
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
                {
                    customerss = customer,
                    preferencess = x
                }).ToList();            
            }
            
            await _customerRep.CreateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Обновление клиента вместе с его предпочтениями
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRep.GetByIdAsync(id);

            if (customer == null) return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;

            var preferences = await _preferenceRep.GetListByListId(request.PreferenceIds);

            if (preferences != null && preferences.Count() > 0)
            {
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
                {
                    customerss = customer,
                    preferencess = x
                }).ToList();
            }

            await _customerRep.UpdateAsync(customer);

            return Ok();

        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRep.GetByIdAsync(id);

            if (customer == null) return NotFound();

            await _customerRep.DeleteAsync(id);

            return Ok();

        }
    }
}