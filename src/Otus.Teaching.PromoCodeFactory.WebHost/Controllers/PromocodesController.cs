﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRep;
        private readonly EfPromoCodeRepository _promoCodeRep;
        private readonly IRepository<Preference> _preferenceRep;

        public PromocodesController(EfPromoCodeRepository promoCodeRep, IRepository<Customer> customerRep, IRepository<Preference> preferenceRep)
        {
            _promoCodeRep = promoCodeRep;
            _customerRep = customerRep;
            _preferenceRep = preferenceRep;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promo = await _promoCodeRep.GetAllAsync();

            var xxx = promo.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd HH:mm:ss"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd HH:mm:ss"),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(xxx);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            var send = new GivePromoCodeReq()
            {
                PartnerName = request.PartnerName,
                Preference = request.Preference,
                PromoCode = request.PromoCode,
                ServiceInfo = request.ServiceInfo
            };
            await _promoCodeRep.GivePromoCodes(send);

            return Ok();

        }
    }
}