﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbContextInit : IUserDbContextInit
    {
        private readonly UserDBcontext _db;

        public DbContextInit(UserDBcontext db)
        {
            _db = db;
        }

        public void UserDbContextInit()
        {
            _db.Database.EnsureDeleted();
            _db.Database.EnsureCreated();

            _db.AddRange(FakeDataFactory.Employees);
            _db.AddRange(FakeDataFactory.Preferences);
            _db.AddRange(FakeDataFactory.Customers);
            _db.SaveChanges();
        }


    }
}
