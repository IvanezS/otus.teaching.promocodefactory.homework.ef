﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class UserDBcontext : DbContext
    {
        public DbSet<Employee> employees { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<Customer> customers { get; set; }
        public DbSet<Preference> preferences { get; set; }
        public DbSet<PromoCode> promoCodes { get; set; }

        public UserDBcontext(DbContextOptions<UserDBcontext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employees").HasKey(x => x.Id);
            modelBuilder.Entity<Employee>().Property(x => x.Email).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(x => x.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(x => x.LastName).HasMaxLength(50);
            modelBuilder.Entity<Employee>().Ignore(x => x.FullName);
            modelBuilder.Entity<Employee>().HasOne(x => x.Role).WithMany().HasForeignKey(x => x.RoleId);

            modelBuilder.Entity<Role>().ToTable("Roles").HasKey(x => x.Id);
            modelBuilder.Entity<Role>().Property(x => x.Name).HasMaxLength(50);

            modelBuilder.Entity<Preference>().ToTable("Preferences").HasKey(x => x.Id);
            modelBuilder.Entity<Preference>().Property(x => x.Name).HasMaxLength(50);


            modelBuilder.Entity<Customer>().ToTable("Customers").HasKey(x => x.Id);
            modelBuilder.Entity<Customer>().Property(x => x.Email).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(x => x.FirstName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Property(x => x.LastName).HasMaxLength(50);
            modelBuilder.Entity<Customer>().Ignore(x => x.FullName);
            modelBuilder.Entity<Customer>().HasMany(x => x.CustomerPreferences).WithOne().HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<PromoCode>().ToTable("Promocodes").HasKey(x => x.Id);
            modelBuilder.Entity<PromoCode>().HasOne(x => x.Preference).WithMany().HasForeignKey(x => x.PreferenceId);
            modelBuilder.Entity<PromoCode>().HasOne(x => x.PartnerManager).WithMany().HasForeignKey(x => x.PartnerManagerId);

            modelBuilder.Entity<CustomerPreference>().ToTable("CustomerPreferences").HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>().HasOne(bc => bc.customerss).WithMany(b => b.CustomerPreferences).HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>().HasOne(bc => bc.preferencess).WithMany(/*b => b.CustomerPreferences*/).HasForeignKey(bc => bc.PreferenceId);

        }
    }
}
