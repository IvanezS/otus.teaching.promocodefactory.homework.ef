﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly UserDBcontext _db;

        public EfRepository(UserDBcontext userContext)
        {
            _db = userContext;
        }

        public async Task CreateAsync(T item)
        {
            var xxx = await _db.Set<T>().AnyAsync(x => x.Id == item.Id);
            if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
            await _db.Set<T>().AddAsync(item);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var xxx = await _db.Set<T>().AnyAsync(x => x.Id == id);
            if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
            _db.Set<T>().Remove(_db.Set<T>().Find(id));
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var xxx= await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return xxx;
        }

        public async Task<List<T>> GetListByListId(List<Guid> ListId)
        {
            return await _db.Set<T>().Where(x => ListId.Contains(x.Id)).ToListAsync();
        }

        public async Task UpdateAsync(T item)
        {
            var xxx = await _db.Set<T>().AnyAsync(x => x.Id == item.Id);
            if (!xxx) throw new ArgumentException($"Объекта с ID = {item.Id} не существует");
            _db.Entry(xxx).CurrentValues.SetValues(item);
            await _db.SaveChangesAsync();
        }
    }
}