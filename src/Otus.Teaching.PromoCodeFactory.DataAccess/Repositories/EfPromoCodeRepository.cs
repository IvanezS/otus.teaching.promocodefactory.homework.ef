﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPromoCodeRepository : IRepository<PromoCode>, IGivePromoCodes
    {
        private readonly UserDBcontext _db;

        public EfPromoCodeRepository(UserDBcontext userContext)
        {
            _db = userContext;
        }

        public async Task CreateAsync(PromoCode item)
        {
            var xxx = await _db.promoCodes.AnyAsync(x => x.Id == item.Id);
            if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
            await _db.promoCodes.AddAsync(item);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var xxx = await _db.promoCodes.AnyAsync(x => x.Id == id);
            if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
            _db.promoCodes.Remove(_db.promoCodes.Find(id));
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
           return await _db.Set<PromoCode>().ToListAsync();
        }

        public async Task<PromoCode> GetByIdAsync(Guid id)
        {
            return await _db.Set<PromoCode>().FirstOrDefaultAsync(x=>x.Id == id);
        }

        public async Task<List<PromoCode>> GetListByListId(List<Guid> ListId)
        {
            return await _db.promoCodes.Where(x => ListId.Contains(x.Id)).ToListAsync();
        }

        public async Task UpdateAsync(PromoCode item)
        {
            _db.Update(item);
            await _db.SaveChangesAsync();
        }

        public async Task GivePromoCodes(GivePromoCodeReq promo)
        {

            var preferences = await _db.preferences.ToListAsync();
            var preference = preferences.ToList().Find(x => x.Name == promo.Preference);

            var employees = await _db.employees.Include(x=>x.Role).ToListAsync();
            var empl = employees.Find(x=>x.FullName == promo.PartnerName);

            var promocode = new PromoCode()
            {
                Code = promo.PromoCode,
                PreferenceId = preference.Id,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1),
                PartnerName = promo.PartnerName,
                ServiceInfo = promo.ServiceInfo,
                PartnerManagerId = empl.Id
               
            };

            await _db.promoCodes.AddAsync(promocode);

            var customers = await _db.customers.Include(x => x.PromoCodes).Include(y => y.CustomerPreferences).ThenInclude(z => z.preferencess).ToListAsync();
            var filteredCustomers = customers.Where(x => x.CustomerPreferences.Select(a => a.PreferenceId).Any(b => b == preference.Id)).ToList();


            foreach (var customer in filteredCustomers)
            {
                if (customer.PromoCodes.All(x => x.Id != promocode.Id))
                {
                    customer.PromoCodes.Add(promocode);
                }
            }

            await _db.SaveChangesAsync();

        }
    }
}
