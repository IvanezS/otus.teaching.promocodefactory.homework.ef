﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : IRepository<Customer>
    {
        private readonly UserDBcontext _db;

        public EfCustomerRepository(UserDBcontext userContext)
        {
            _db = userContext;
        }

        public async Task CreateAsync(Customer item)
        {
            var xxx = await _db.customers.AnyAsync(x => x.Id == item.Id);
            if (xxx) throw new ArgumentException($"Объект с ID = {item.Id} уже существует");
            await _db.customers.AddAsync(item);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var xxx = await _db.customers.AnyAsync(x => x.Id == id);
            if (!xxx) throw new ArgumentException($"Объекта с ID = {id} не существует");
            _db.customers.Remove(_db.customers.Find(id));
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _db.customers.Include(x => x.PromoCodes).Include(y => y.CustomerPreferences).ThenInclude(z => z.preferencess).ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _db.customers.Include(customer => customer.PromoCodes).Include(customer => customer.CustomerPreferences).ThenInclude(preference => preference.preferencess).FirstOrDefaultAsync(x=>x.Id == id);
        }

        public async Task<List<Customer>> GetListByListId(List<Guid> ListId)
        {
            return await _db.customers.Where(x => ListId.Contains(x.Id)).ToListAsync();
        }

        public async Task UpdateAsync(Customer item)
        {
            _db.Update(item);
            await _db.SaveChangesAsync();
        }
    }
}
